<?php $titre ="Classement"; ?>
<?php ob_start();
require "bdd/bddconfig.php"; 
session_start();

try {
    $objBdd = new PDO("mysql:host=$bddserver;
   dbname=$bddname;
   charset=utf8",$bddlogin, $bddpass);

    $objBdd->setAttribute(PDO::ATTR_ERRMODE,
   PDO::ERRMODE_EXCEPTION);

   $listeClass = $objBdd->query("SELECT * FROM classebateau order by typeCoque");
   }
catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
   }
?>         
     <article>                
        <h2>Classements</h2>
        <?php 
        foreach ($listeClass as $classe) { ?>
                <li>
                <span><?php echo $classe['typeCoque'] ?></span>
                    <a href="listebateaux.php?idClasse=<?= $classe['idClasse']; ?>">
                    <span><?php echo $classe['nomClasse']; ?></span>
                    </a>
                </li>
            <?php } ?>
    </article>
<?php $contenu = ob_get_clean(); ?>            
<?php require 'gabarit/template.php' ?>
