<?php $titre ="detailBateaux"; ?>
<?php ob_start();
require "bdd/bddconfig.php"; 
session_start();
if (isset($_GET['idBateau'])) {
    $idBateau= intval(htmlspecialchars($_GET['idBateau']));
}
try {
    $objBdd = new PDO("mysql:host=$bddserver;
   dbname=$bddname;
   charset=utf8",$bddlogin,$bddpass);

    $listeSkipper = $objBdd->prepare("SELECT * FROM skipper
                                    Where idBateau = $idBateau");
    $listeSkipper->execute();

    $listeBateau = $objBdd->prepare("SELECT * FROM bateau
                                    Where idBateau = $idBateau");
    $listeBateau->execute();
   }
   catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}

?>
<article>
<?php 

   foreach ($listeSkipper as $skipper) { ?>
            
            <li><img src="images/skippers/<?php echo $skipper['photo'] ?>" alt=""></li>
            <li><?php echo $skipper['nomSkipper']; ?></li>
        <?php } ?>

        <?php     
        foreach ($listeBateau as $bateau) { ?>
            
            <li><img src="images/bateaux/<?php echo $bateau['photo'] ?>" alt=""></li>
            <li><?php echo $bateau['nomBateau']; ?></li>
        <?php } ?>

    </article>
<?php $contenu = ob_get_clean(); ?>            
<?php require 'gabarit/template.php' ?>