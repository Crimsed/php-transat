<?php 
require "bdd/bddconfig.php";              
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?= $titre; ?></title>
        <link rel="stylesheet" href="css/transat.css" />
    </head>

    <body>
        <div id="conteneur">
            <header>

            <?php
            
            if (isset($_SESSION['logged_in']['login']) == TRUE) {
            //l'internaute est authentifié
            echo $_SESSION['logged_in']['prenom'].' '.$_SESSION['logged_in']['nom'];
            //affichage "Se déconnecter"(logout.php), "Prif", "Paramètres", etc ...
            ?>
            <a  href="logout.php">déconnection</a>
            <?php
            } else { 
            //Personne n'est authentifié 
            // affichage d'un lien pour se connecter
            ?>
            <div class='login'>
                    <form method="POST" action="login_action.php">
                        <fieldset>
                            <legend>Connectez-vous</legend>
                            <input type="text" name="login" value="" placeholder="Login" required>
                            <input type="text" name="password" value="" placeholder="Password" required>
                            <input type="submit" value="Valider">
                        </fieldset>
                    </form>   
                </div>
            
            <?php               
}?>

            </header>
            <nav>
                <ul>
                    <li><a href="index.php" class="navitem">Accueil</a></li>
                    <li><a href="classements.php" class="navitem">Classements</a></li>
                    <?php   if (isset($_SESSION['logged_in']['login']) == TRUE) { ?>
                    <li><a href="ajoutclasse.php" class="navitemprivate">Ajout Classe</a></li>
                    <?php }?>
                </ul>
            </nav>
            <section>
                <?php echo $contenu; ?>
            </section>
            <footer>
                <p>Copyright Moi - Tous droits réservés - 
                    <a href="#">Contact</a></p>
            </footer>
        </div>    
    </body>
</html>