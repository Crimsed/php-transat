<?php $titre ="listebateaux"; ?>
<?php ob_start();
require "bdd/bddconfig.php"; 
session_start();
if (isset($_GET['idClasse'])) {
    $idClasse= intval(htmlspecialchars($_GET['idClasse']));
}
try {
    $objBdd = new PDO("mysql:host=$bddserver;
   dbname=$bddname;
   charset=utf8",$bddlogin,$bddpass);

    $listeBateau = $objBdd->prepare("SELECT * FROM bateau
                                    Where idClasse = $idClasse
                                    order by classementFinal");
    $listeBateau->execute();
   }
   catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}

?>
<article>
<?php 
        foreach ($listeBateau as $bateau) { ?>
                <li>
                <span>
                <?php 
                if ($bateau['classementFinal'] == '9999') {
                    $bateau['classementFinal'] = "AB";
                    echo $bateau['classementFinal'];
                }
                else {
                    echo $bateau['classementFinal']; 
                 }?>
                 <a href="detailbateaux.php?idBateau=<?= $bateau['idBateau']; ?>">
                <?php echo $bateau['nomBateau']; ?>
                </a></span>
                </li>
            <?php } ?>
    </article>
<?php $contenu = ob_get_clean(); ?>            
<?php require 'gabarit/template.php' ?>