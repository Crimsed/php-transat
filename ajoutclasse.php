<?php $titre ="Ajout classe"; ?>
<?php ob_start();
require "bdd/bddconfig.php"; 
session_start();
if (isset($_SESSION['logged_in']['login']) !== TRUE) {
    // Redirige vers la page d'accueil (ou login.php) si pas authentifié
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
    $page = 'index.php';
    header("Location: http://$serveur$chemin/$page");
}


?>         
     <article>                
     <h1>Ajouter une nouvelle classe</h1>
    <form method="POST" action="ajoutclasse_action.php">
        <fieldset>
            <legend>Caractéristiques</legend>
            Nom :<br />
            <input type="text" name="nomClasse" value="" placeholder="Nom de la classe" required>
            <br />
            Taille de la coque :<br>
            <input type="text" name="tailleCoque" value="" placeholder="Taille de la coque" required>
            <br />
            <input type="submit" value="Enregistrer">
        </fieldset>
    </form>
</article>
    </article>
<?php $contenu = ob_get_clean(); ?>            
<?php require 'gabarit/template.php' ?>
